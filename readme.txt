Read Me

Data Conversions - describes the enumeration of strings in play by play and results data

NFL by Play - full data set of plays

NFL by Play with Removed Attributes - removed columns that aren't useful

NFL Game Results - full data set of game results

NFL Game Results with Removed Columns - removed columns that aren't useful

nflData - MATLAB readable version of NFL by Play with Removed Attributes

nflResults - MATLAB readable version of NFL Game Results with Removed Columns

nflPredictor - MATLAB script for extracting scores and statistics