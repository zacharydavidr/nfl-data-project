%denote specific columns of interest from the input file
gameIdAttrCol = 1;

yardLineAttrCol = 9;
posTeamAttrCol = 14;
defTeamAttrCol = 15;
yardsGainedAttrCol = 17;
passAttAttrCol = 27;
passOutcomeAttrCol = 28;
passLengthAttrCol = 29;
passLocAttrCol = 33;
intThrownAttrCol = 34;
rushAttAttrCol = 35;
rushLocAttrCol = 36;
rushGapAttrCol = 37;
fumAttrCol = 42;
fumTeamAttrCol = 43;
sackAttrCol = 44;
penAccAttrCol = 47;
penYardAttrCol = 49;
posScoreAttrCol = 50;
defScoreAttrCol = 51;
scoreDiffAttrCol = 52;
absDiffAttrCol = 53;
homeTeamAttrCol = 54;
awayTeamAttrCol = 55;
seasonAttrCol = 63;

%read in the plays and results
playData = int32(table2array(readtable('nflData.csv','ReadVariableNames',false)));
resultsData = int32(table2array(readtable('nflResults.csv','ReadVariableNames',false)));
resultsData = resultsData(1:4:end,:);
gameSeasons = unique(playData(:,[gameIdAttrCol seasonAttrCol]),'rows');
resultsData = [resultsData gameSeasons(:,2)];

%add on the 2017 test data
playData = [playData; int32(table2array(readtable('2017data.csv','ReadVariableNames',false)))];
results2017 = int32(table2array(readtable('2017results.csv','ReadVariableNames',false)));
results2017 = [results2017 2017*ones(size(results2017,1),1)];
resultsData = [resultsData; results2017];

[numPlays,numPlayAttributes] = size(playData);

%fix the weird format of the plays data where non-plays don't have an
%actual score
for playIdx = 2:numPlays
    if playData(playIdx,posScoreAttrCol) == -1
        scoreUpdateIndices = [posScoreAttrCol defScoreAttrCol scoreDiffAttrCol absDiffAttrCol];
        playData(playIdx,scoreUpdateIndices) = playData(playIdx-1,scoreUpdateIndices);
    end
end

gameIds = unique(playData(:,gameIdAttrCol));
numGames = numel(gameIds);
numTeams = 32;
numSeasons = 9;
numGamesPerTeamPerSeason = 16;
numInterestAttributes = 41;

teamData = zeros(numSeasons,numTeams,numGamesPerTeamPerSeason,numInterestAttributes);
currSeason = 0;
gameNumPerTeam = [];

gameIdColumn = playData(:,gameIdAttrCol);

for gameIdIdx = 1:numGames
    
    %extract all the plays for a specific game
    currentGameData = playData(gameIdColumn==gameIds(gameIdIdx), :);
    
    %determine the season for this game
    currGameSeason = currentGameData(1,seasonAttrCol) - 2008;
    if currGameSeason ~= currSeason %start of a new season
        currSeason = currGameSeason;
        gameNumPerTeam = zeros(1,numTeams);
    end
    
    %find the home and away team for this game
    homeTeam = currentGameData(1,homeTeamAttrCol);
    awayTeam = currentGameData(1,awayTeamAttrCol);
    
    %find what game # this is for each team this season
    currGameTeams = zeros(1,numTeams);
    currGameTeams([homeTeam awayTeam]) = 1;
    gameNumPerTeam = gameNumPerTeam + currGameTeams;
    
    %iterate thru all of the plays
    for playIdx = 1:size(currentGameData,1)
        currPlay = currentGameData(playIdx,:);
        
        %determine which team is offense vs defense
        posTeam = currPlay(posTeamAttrCol);
        defTeam = currPlay(defTeamAttrCol);
        
        %determine if the play actually happened based on specific
        %conditions relating to if a penalty canceled the play
        playStood = posTeam~=0 && defTeam~=0;
        if currPlay(penAccAttrCol)==1
            currYardLine = currPlay(yardLineAttrCol);
            nextYardLine = 0;
            if playIdx ~= size(currentGameData,1)
                nextYardLine = currentGameData(playIdx+1,yardLineAttrCol);
            end
            if abs(nextYardLine - currYardLine) == currPlay(penYardAttrCol)
                playStood = 0;
            end
        end
        
        if playStood
            
            %determine all of the relevant stats to account for on this
            %play
            currPlayData = zeros(1,1,1,floor(numInterestAttributes/2));
            posTeamGameNum = gameNumPerTeam(posTeam);
            defTeamGameNum = gameNumPerTeam(defTeam);
            
            if currPlay(passAttAttrCol) == 1 %pass was attempted in this play
                %add pass yards, pass completions/attempts, pass location,
                %and pass depth
                currPlayData(1,1,1,1) = currPlay(yardsGainedAttrCol);
                currPlayData(1,1,1,3) = currPlay(passOutcomeAttrCol);
                currPlayData(1,1,1,4) = 1;
                currPlayData(1,1,1,6) = currPlay(intThrownAttrCol);
                if currPlay(passLengthAttrCol) == 0
                    currPlayData(1,1,1,9) = 1;
                else
                    currPlayData(1,1,1,10) = 1;
                end
                if currPlay(passLocAttrCol) == 1
                    currPlayData(1,1,1,11) = 1;
                elseif currPlay(passLocAttrCol) == 2
                    currPlayData(1,1,1,12) = 1;
                elseif currPlay(passLocAttrCol) == 3
                    currPlayData(1,1,1,13) = 1;
                end
            end
            
            if currPlay(rushAttAttrCol) == 1 %rush was attempted in this play
                %add rush yards, rushes, rush location, and rush gap
                currPlayData(1,1,1,2) = currPlay(yardsGainedAttrCol);
                currPlayData(1,1,1,5) = 1;
                if currPlay(rushLocAttrCol) == 1
                    currPlayData(1,1,1,18) = 1;
                elseif currPlay(rushLocAttrCol) == 2
                    currPlayData(1,1,1,14) = 1;
                    currPlayData(1,1,1,19) = 1;
                elseif currPlay(rushLocAttrCol) == 3
                    currPlayData(1,1,1,20) = 1;
                end
                if currPlay(rushGapAttrCol) == 1
                    currPlayData(1,1,1,15) = 1;
                elseif currPlay(rushGapAttrCol) == 2
                    currPlayData(1,1,1,16) = 1;
                elseif currPlay(rushGapAttrCol) == 3
                    currPlayData(1,1,1,17) = 1;
                end
            end
            
            if currPlay(fumAttrCol) == 1 %there was a fumble
                if currPlay(fumTeamAttrCol) == defTeam %the fumble was lost
                    currPlayData(1,1,1,7) = 1;
                end
            end
            
            if currPlay(sackAttrCol) == 1 %there was a sack
                currPlayData(1,1,1,8) = 1;
            end
            
            %update the game offensive stats for the offensive team, and
            %the defensive stats for the defensive team
            posPlay = cat(4, cat(4, currPlayData ,zeros(1,1,1,floor(numInterestAttributes/2))), 0);
            defPlay = cat(4, cat(4, zeros(1,1,1,floor(numInterestAttributes/2)), currPlayData), 0);
            teamData(currSeason,posTeam,posTeamGameNum,:) = teamData(currSeason,posTeam,posTeamGameNum,:) + posPlay;
            teamData(currSeason,defTeam,defTeamGameNum,:) = teamData(currSeason,defTeam,defTeamGameNum,:) + defPlay;
        end
    end
    
    %mark that this game was played
    teamData(currSeason,homeTeam,gameNumPerTeam(homeTeam),numInterestAttributes) = 1;
    teamData(currSeason,awayTeam,gameNumPerTeam(awayTeam),numInterestAttributes) = 1;
end

runningTeamData = zeros(numTeams, numInterestAttributes+2);
currSeason = 0;
gameData = zeros(numGames, (numInterestAttributes+2) * 2 + 2); %home data, away data, classifier (1 = Home Win, -1 = Away Win, 0 = Tie), spread

%aggregate all of the games per season
for gameIndex = 1:numGames
    currMatchup = resultsData(gameIndex,:);
    gameSeason = currMatchup(6) - 2008;
    if gameSeason ~= currSeason
        currSeason = gameSeason;
        runningTeamData = zeros(numTeams, numInterestAttributes+2);
    end
    homeTeam = currMatchup(2);
    awayTeam = currMatchup(3);
    
    gameResult = sign(currMatchup(4)-currMatchup(5));
    gameSpread = currMatchup(4)-currMatchup(5);
    gameData(gameIndex,:) = [runningTeamData(homeTeam,:) runningTeamData(awayTeam,:) gameResult gameSpread];
    
    %add the home game stats for the home team and away game stats for away
    %team
    homeGameNum = runningTeamData(homeTeam,numInterestAttributes) + 1;
    homeTeamGameStats = [squeeze(teamData(currSeason,homeTeam,homeGameNum,:)); 0; 0]';
    homeTeamGameStats(numInterestAttributes+1) = currMatchup(4);
    homeTeamGameStats(numInterestAttributes+2) = currMatchup(5);
    runningTeamData(homeTeam,:) = runningTeamData(homeTeam,:) + homeTeamGameStats;
    awayGameNum = runningTeamData(awayTeam,numInterestAttributes) + 1;
    awayTeamGameStats = [squeeze(teamData(currSeason,awayTeam,awayGameNum,:)); 0; 0]';
    awayTeamGameStats(numInterestAttributes+1) = currMatchup(5);
    awayTeamGameStats(numInterestAttributes+2) = currMatchup(4);
    runningTeamData(awayTeam,:) = runningTeamData(awayTeam,:) + awayTeamGameStats;
end

%these are just used so we don't end up using the 0 games played statistics
%for normalizing the data
homeGameCol = gameData(:,41);
awayGameCol = gameData(:,84);

%divide per/game statistics by the number of games (yards, points, etc.)
for gameDivAttr = [1:8 21:28 42:43]
    gameData(homeGameCol~=0,gameDivAttr) = gameData(homeGameCol~=0,gameDivAttr)./homeGameCol(homeGameCol~=0);
    gameData(awayGameCol~=0,gameDivAttr+numInterestAttributes+2) = gameData(awayGameCol~=0,gameDivAttr+numInterestAttributes+2)./awayGameCol(awayGameCol~=0);
end

%divide the pass length numbers by total passes to get ratios
for passLenAttr = [9 29]
    passLenSumCol = gameData(:,passLenAttr) + gameData(:,passLenAttr+1);
    gameData(homeGameCol~=0,passLenAttr) = gameData(homeGameCol~=0,passLenAttr)./passLenSumCol(homeGameCol~=0);
    gameData(homeGameCol~=0,passLenAttr+1) = gameData(homeGameCol~=0,passLenAttr+1)./passLenSumCol(homeGameCol~=0);
    passLenSumCol = gameData(:,passLenAttr+numInterestAttributes+2) + gameData(:,passLenAttr+1+numInterestAttributes+2);
    gameData(awayGameCol~=0,passLenAttr+numInterestAttributes+2) = gameData(awayGameCol~=0,passLenAttr+numInterestAttributes+2)./passLenSumCol(awayGameCol~=0);
    gameData(awayGameCol~=0,passLenAttr+1+numInterestAttributes+2) = gameData(awayGameCol~=0,passLenAttr+1+numInterestAttributes+2)./passLenSumCol(awayGameCol~=0);
end

%divide the pass location numbers by total passes to get ratios
for passLocAttr = [11 31]
    passLocSumCol = gameData(:,passLocAttr) + gameData(:,passLocAttr+1) + gameData(:,passLocAttr+2);
    gameData(homeGameCol~=0,passLocAttr) = gameData(homeGameCol~=0,passLocAttr)./passLocSumCol(homeGameCol~=0);
    gameData(homeGameCol~=0,passLocAttr+1) = gameData(homeGameCol~=0,passLocAttr+1)./passLocSumCol(homeGameCol~=0);
    gameData(homeGameCol~=0,passLocAttr+2) = gameData(homeGameCol~=0,passLocAttr+2)./passLocSumCol(homeGameCol~=0);
    passLocSumCol = gameData(:,passLocAttr+numInterestAttributes+2) + gameData(:,passLocAttr+1+numInterestAttributes+2) + gameData(:,passLocAttr+2+numInterestAttributes+2);
    gameData(awayGameCol~=0,passLocAttr+numInterestAttributes+2) = gameData(awayGameCol~=0,passLocAttr+numInterestAttributes+2)./passLocSumCol(awayGameCol~=0);
    gameData(awayGameCol~=0,passLocAttr+1+numInterestAttributes+2) = gameData(awayGameCol~=0,passLocAttr+1+numInterestAttributes+2)./passLocSumCol(awayGameCol~=0);
    gameData(awayGameCol~=0,passLocAttr+2+numInterestAttributes+2) = gameData(awayGameCol~=0,passLocAttr+2+numInterestAttributes+2)./passLocSumCol(awayGameCol~=0);
end

%divide the rush gap numbers by total rushes to get ratios
for rushGapAttr = [14 34]
    rushGapSumCol = gameData(:,rushGapAttr) + gameData(:,rushGapAttr+1) + gameData(:,rushGapAttr+2) + gameData(:,rushGapAttr+3);
    gameData(homeGameCol~=0,rushGapAttr) = gameData(homeGameCol~=0,rushGapAttr)./rushGapSumCol(homeGameCol~=0);
    gameData(homeGameCol~=0,rushGapAttr+1) = gameData(homeGameCol~=0,rushGapAttr+1)./rushGapSumCol(homeGameCol~=0);
    gameData(homeGameCol~=0,rushGapAttr+2) = gameData(homeGameCol~=0,rushGapAttr+2)./rushGapSumCol(homeGameCol~=0);
    gameData(homeGameCol~=0,rushGapAttr+3) = gameData(homeGameCol~=0,rushGapAttr+3)./rushGapSumCol(homeGameCol~=0);
    rushGapSumCol = gameData(:,rushGapAttr+numInterestAttributes+2) + gameData(:,rushGapAttr+1+numInterestAttributes+2) + gameData(:,rushGapAttr+2+numInterestAttributes+2) + gameData(:,rushGapAttr+3+numInterestAttributes+2);
    gameData(awayGameCol~=0,rushGapAttr+numInterestAttributes+2) = gameData(awayGameCol~=0,rushGapAttr+numInterestAttributes+2)./rushGapSumCol(awayGameCol~=0);
    gameData(awayGameCol~=0,rushGapAttr+1+numInterestAttributes+2) = gameData(awayGameCol~=0,rushGapAttr+1+numInterestAttributes+2)./rushGapSumCol(awayGameCol~=0);
    gameData(awayGameCol~=0,rushGapAttr+2+numInterestAttributes+2) = gameData(awayGameCol~=0,rushGapAttr+2+numInterestAttributes+2)./rushGapSumCol(awayGameCol~=0);
    gameData(awayGameCol~=0,rushGapAttr+3+numInterestAttributes+2) = gameData(awayGameCol~=0,rushGapAttr+3+numInterestAttributes+2)./rushGapSumCol(awayGameCol~=0);
end

%divide the rush location numbers by total rushes to get ratios
for rushLocAttr = [18 38]
    rushLocSumCol = gameData(:,rushLocAttr) + gameData(:,rushLocAttr+1) + gameData(:,rushLocAttr+2);
    gameData(homeGameCol~=0,rushLocAttr) = gameData(homeGameCol~=0,rushLocAttr)./rushLocSumCol(homeGameCol~=0);
    gameData(homeGameCol~=0,rushLocAttr+1) = gameData(homeGameCol~=0,rushLocAttr+1)./rushLocSumCol(homeGameCol~=0);
    gameData(homeGameCol~=0,rushLocAttr+2) = gameData(homeGameCol~=0,rushLocAttr+2)./rushLocSumCol(homeGameCol~=0);
    rushLocSumCol = gameData(:,rushLocAttr+numInterestAttributes+2) + gameData(:,rushLocAttr+1+numInterestAttributes+2) + gameData(:,rushLocAttr+2+numInterestAttributes+2);
    gameData(awayGameCol~=0,rushLocAttr+numInterestAttributes+2) = gameData(awayGameCol~=0,rushLocAttr+numInterestAttributes+2)./rushLocSumCol(awayGameCol~=0);
    gameData(awayGameCol~=0,rushLocAttr+1+numInterestAttributes+2) = gameData(awayGameCol~=0,rushLocAttr+1+numInterestAttributes+2)./rushLocSumCol(awayGameCol~=0);
    gameData(awayGameCol~=0,rushLocAttr+2+numInterestAttributes+2) = gameData(awayGameCol~=0,rushLocAttr+2+numInterestAttributes+2)./rushLocSumCol(awayGameCol~=0);
end

%separate into training data and test data
gameData2017 = gameData(2049:end,:);
gameData = gameData(1:2048,:);

%remove tie games, we only want to classify wins and losses
nonTieGames = gameData(:,87)~=0;
gameData = gameData(nonTieGames,:);

rng(1);
predictionModel = TreeBagger(50,gameData(:,1:86),gameData(:,87),...
    'OOBPrediction','On','Method','classification');

spreadModel = TreeBagger(50,gameData(:,1:86),gameData(:,88),...
    'OOBPrediction','On','Method','regression');

predictions2017 = int32(cellfun(@str2num,predict(predictionModel,gameData2017(:,1:86))));
spreads2017 = predict(spreadModel,gameData2017(:,1:86));
predictionError = mean(predictions2017~=gameData2017(:,87));
homeError = mean(ones(size(gameData2017(:,87)))~=gameData2017(:,87));

disp(predictionError);
disp(homeError);

allPredictions = zeros(32);
allSpreads = zeros(32);
for homeTeam = 1:32
    for awayTeam = 1:32
        if homeTeam ~= awayTeam
            [prediction,spread] = matchupPrediction(predictionModel,spreadModel,homeTeam,awayTeam,runningTeamData);
            allPredictions(homeTeam,awayTeam) = prediction;
            allSpreads(homeTeam,awayTeam) = spread;
        end
    end
end

teamWinCountsAbove500 = sum(allPredictions') - sum(allPredictions);
teamQuality = floor(tiedrank(teamWinCountsAbove500*-1));