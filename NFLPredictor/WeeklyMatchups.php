<?PHP

$current_week = 3;


const PIT = 1-1;
const BAL = 2-1;
const CIN = 3-1;
const CLE = 4-1;
const NE = 5-1;
const MIA = 6-1;
const NYJ = 7-1;
const BUF = 8-1;
const JAX = 9-1;
const IND = 10-1;
const HOU = 11-1;
const TEN = 12-1;
const SD = 13-1;
const DEN = 14-1;
const OAK = 15-1;
const KC = 16-1;
const MIN = 17-1;
const GB = 18-1;
const CHI = 19-1;
const DET = 20-1;
const PHI = 21-1;
const WAS = 22-1;
const DAL = 23-1;
const NYG = 24-1;
const TB = 25-1;
const NO = 26-1;
const CAR = 27-1;
const ATL = 28-1;
const SEA = 29-1;
const SF = 30-1;
const STL = 31-1;
const ARI = 32-1;


$win_loss_matrix = array();

if (($handle = fopen("win_loss_matrix.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        array_push($win_loss_matrix, $data);
    }
    fclose($handle);
}

$spreads = array();

if (($handle = fopen("spreads_matrix.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        array_push($spreads, $data);
    }
    fclose($handle);
}



function getTeamName($index){
    $teams = array("Pittsburgh Steelers", "Baltimore Ravens", "Cincinnati Bengals", "Cleveland Browns",
        "New England Patriots","Miami Dolphins", "New York Jets", "Buffalo Bills", "Jacksonville Jaguars",
        "Indianapolis Colts", "Houston Texans", "Tennessee Titans", "Los Angeles Chargers", "Denver Broncos",
        "Oakland Raiders", "Kansas City Chiefs ", "Minnesota Vikings", "Green Bay Packers", "Chicago Bears",
        "Detroit Lions", "Philadelphia Eagles", "Washington Redskins", "Dallas Cowboys", "New York Giants",
        "Tampa Bay Buccaneers", "New Orleans Saints", "Carolina Panthers", "Atlanta Falcons", "Seattle Seahawks",
        "San Francisco 49ers", "Los Angeles Rams", "Arizona Cardinals");
    return $teams[$index];
}

$matchups_11 = array(array(TEN,PIT),array(DET,CHI),array(JAX,CLE),array(BAL,GB), array(TB, MIA),
    array(STL,MIN),array(WAS,NO),array(KC,NYG),array(ARI,HOU),array(BUF,SD), array(CIN,DEN),
    array(NE,OAK),array(PHI,DAL),array(ATL,SEA));
$matchups_11_spreads = array(7,-3,-7,-2.5,-1,-1.5,9.5,-9.5,2.5,7,2.5,-7,-6,-1);


$matchups_12 = array(array(MIN,DET),array(SD,DAL),array(NYG,WAS),array(MIA,NE), array(CAR, NYJ),
    array(CHI,PHI),array(TEN,IND),array(BUF,KC),array(TB,ATL),array(CLE,CIN), array(SEA,SF),
    array(DEN,OAK),array(JAX,ARI),array(NO,STL),array(GB,PIT),array(HOU,BAL));
$matchups_12_spreads = array(-2.5,-1,7,16.5,-5.5,13.5,-3,8,10,7.5,-6.5,4,-6,2.5,14,7.5);


$matchups_13 = array(array(WAS,DAL),array(MIN,ATL),array(DET,BAL), array(NE, BUF),
array(SF,CHI),array(TB,GB),array(IND,JAX),array(DEN,MIA),array(CAR,NO), array(KC,NYJ),
array(HOU,TEN),array(CLE,SD),array(STL,ARI),array(NYG,OAK),array(PHI,SEA),array(PIT,CIN));
$matchups_13_spreads = array(-1.5,2,2.5,-7.5,2.5,3,10,-1.5,6,-3.5,7,13,-7,10,-4,-4);

$matchups_14 = array(array(NO,ATL),array(IND,BUF), array(MIN, CAR),array(SF,HOU),
    array(CHI,CIN),array(GB,CLE),array(SEA,JAX),array(OAK,KC),array(DET,TB), array(TEN,ARI),
    array(NYJ,DEN),array(WAS,SD),array(PHI,STL),array(DAL,NYG),array(BAL,PIT),array(NE,MIA));
$matchups_14_spreads = array(-1.5,"NA",-3,3,7,-3.5,3,4,"NA",-3,-1.5,6,2.5,-4.5,7,-11);


$matchups_15 =  array(array(DEN,IND),array(CHI,DET),array(SD,KC), array(MIA, BUF),
    array(GB,CAR),array(BAL,CLE),array(HOU,JAX),array(CIN,MIN),array(NYJ,NO), array(PHI,NYG),
    array(ARI,WAS),array(STL,SEA),array(NE,PIT),array(TEN,SF),array(DAL,OAK),array(ATL,TB));

$matchups_16 =  array(array(IND,BAL),array(MIN,GB),array(TB,CAR), array(CLE, CHI),
    array(DET,CIN),array(MIA,KC),array(BUF,NE),array(ATL,NO),array(SD,NYJ), array(STL,TEN),
    array(DEN,WAS),array(JAX,SF),array(NYG,ARI),array(SEA,DAL),array(PIT,HOU),array(OAK,PHI));

$matchups_17 =  array(array(CAR,ATL),array(CIN,BAL),array(GB,DET), array(HOU, IND),
    array(BUF,MIA),array(CHI,MIN),array(NYJ,NE),array(WAS,NYG),array(DAL,PHI), array(CLE,PIT),
    array(NO,TB),array(JAX,TEN),array(KC,DEN),array(OAK,SD),array(SF,STL),array(ARI,SEA));


$matchups = array($matchups_11, $matchups_12, $matchups_13, $matchups_14, $matchups_15, $matchups_16, $matchups_17);
$vegas_spreads = array($matchups_11_spreads, $matchups_12_spreads, $matchups_13_spreads, $matchups_14_spreads);
?>

<!doctype html>
<html lang="en">
<head>
    <link href="design.css" type="text/css" rel="stylesheet" />
    <meta charset="utf-8">
    <title>NFL Predictor</title>
</head>


<body>

    <ul>
        <li><a class="active" href="WeeklyMatchups.php">Weekly Matchups</a></li>
        <li><a href="TeamComparison.php">Team Comparison</a></li>
        <li><a href="About.php">About</a></li>
    </ul>


    <?php
    if (isset($_POST['week'])) {
        $week_selector = $_POST['week'];
    } else{
        $week_selector = 0;
    }
    ?>

    <p>
        <form method="post" action="" name="form">
            <select id="week" name="week">
                <option value="0">Week 11</option>
                <option value="1">Week 12</option>
                <option value="2">Week 13</option>
                <option value="3">Week 14</option>
                <option value="4">Week 15</option>
                <option value="5">Week 16</option>
                <option value="6">Week 17</option>
            </select>
            <input name="submit" type="submit" value="Select">
        </form>
    </p>

    <script type="text/javascript">
        document.getElementById('week').value = "<?php echo $_POST['week'];?>";
    </script>


    <table>
        <tr align="center">
            <th>Away</th>
            <th>Home</th>
            <th>Predicted Winner</th>
            <th>Predicted Spread</th>
            <th>Vegas Spread</th>
            <th>Result</th>

        </tr>

        <?php
        $index = 0;
        foreach ($matchups[$week_selector] as $matchup){
            $away_team = $matchup[0];
            $home_team = $matchup[1];

            echo "<tr align='center'>";
            echo "<td>" . getTeamName($away_team) . "</td>";
            echo "<td>" . getTeamName($home_team) . "</td>";
            echo "<td>";
            if($win_loss_matrix[$home_team][$away_team] == 1)
                echo "Home";
            else
                echo "Away";
            echo"</td>";

            echo "<td>" . $spreads[$home_team][$away_team] . "</td>";

            if($week_selector <= $current_week){
                echo "<td>" . $vegas_spreads[$week_selector][$index] . "</td>";
                $temp_spread = ((float)$spreads[$home_team][$away_team]-(float)$vegas_spreads[$week_selector][$index]);
                if(abs($temp_spread) < 5){
                    $confidence = "low";
                }elseif(abs($temp_spread) < 10){
                    $confidence = "medium";
                }else{
                    $confidence = "high";
                }

                if($temp_spread > 0){
                    echo "<td class='$confidence'>" . "+" . $temp_spread . "</td>";
                }else{
                    echo "<td class='$confidence'>" . $temp_spread . "</td>";

                }
            }else{
                echo "<td>NA</td>";
                echo "<td>NA</td>";
            }
            echo "</tr>";
            $index++;
        }

        ?>
    </table>

</body>
</html>