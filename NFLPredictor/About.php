<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Team Comparison</title>
    <link href="design.css" type="text/css" rel="stylesheet" />

</head>
<body>

<header>
    <ul>
        <li><a href="WeeklyMatchups.php">Weekly Matchups</a></li>
        <li><a href="TeamComparison.php">Team Comparison</a></li>
        <li><a class="active" href="About.php">About</a></li>
    </ul>
</header>
<p>This is a CSE 881 Dating Mining Project. The goal of this project is to analyze
multiple seasons worth of play by play NFL data to determine the likely winner of NFL games.</p>
<p>Andrew Pollack, Zachary Ray, and Thrilok Nagamalla</p>

</body>
</html>