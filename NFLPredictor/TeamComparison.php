<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Team Comparison</title>
    <link href="design.css" type="text/css" rel="stylesheet" />

</head>
<body>

<header>
    <ul>
        <li><a href="WeeklyMatchups.php">Weekly Matchups</a></li>
        <li><a class="active" href="TeamComparison.php">Team Comparison</a></li>
        <li><a href="About.php">About</a></li>
    </ul>
</header>

<p>Select two teams to view the predicted results!</p>

<?php
const PIT = 1-1;
const BAL = 2-1;
const CIN = 3-1;
const CLE = 4-1;
const NE = 5-1;
const MIA = 6-1;
const NYJ = 7-1;
const BUF = 8-1;
const JAX = 9-1;
const IND = 10-1;
const HOU = 11-1;
const TEN = 12-1;
const SD = 13-1;
const DEN = 14-1;
const OAK = 15-1;
const KC = 16-1;
const MIN = 17-1;
const GB = 18-1;
const CHI = 19-1;
const DET = 20-1;
const PHI = 21-1;
const WAS = 22-1;
const DAL = 23-1;
const NYG = 24-1;
const TB = 25-1;
const NO = 26-1;
const CAR = 27-1;
const ATL = 28-1;
const SEA = 29-1;
const SF = 30-1;
const STL = 31-1;
const ARI = 32-1;

function getTeamName($index){
    $teams_list = array("Pittsburgh Steelers", "Baltimore Ravens", "Cincinnati Bengals", "Cleveland Browns",
        "New England Patriots","Miami Dolphins", "New York Jets", "Buffalo Bills", "Jacksonville Jaguars",
        "Indianapolis Colts", "Houston Texans", "Tennessee Titans", "Los Angeles Chargers", "Denver Broncos",
        "Oakland Raiders", "Kansas City Chiefs ", "Minnesota Vikings", "Green Bay Packers", "Chicago Bears",
        "Detroit Lions", "Philadelphia Eagles", "Washington Redskins", "Dallas Cowboys", "New York Giants",
        "Tampa Bay Buccaneers", "New Orleans Saints", "Carolina Panthers", "Atlanta Falcons", "Seattle Seahawks",
        "San Francisco 49ers", "Los Angeles Rams", "Arizona Cardinals");
    return $teams_list[$index];
}

//$teams = array(PIT, BAL, CIN,CLE,NE,MIA,NYJ,BUF,JAX,IND,HOU,TEN,SD,
 //   DEN,OAK, KC,MIN, GB,CHI,DET,PHI,WAS,DAL,NYG, TB,NO,CAR,ATL,SEA,SF,STL,ARI);

$teams= array(ARI,ATL,BAL,BUF,CAR,CHI,CIN,CLE,DAL,DEN,DET,GB,HOU,IND,JAX,KC,MIA,MIN,NE,NO,NYG,NYJ,OAK,
PHI,PIT,SD,SEA,SF,STL,TB,TEN,WAS);

$win_loss_matrix = array();

if (($handle = fopen("win_loss_matrix.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        array_push($win_loss_matrix, $data);
    }
    fclose($handle);
}

$spreads = array();

if (($handle = fopen("spreads_matrix.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        array_push($spreads, $data);
    }
    fclose($handle);
}

?>

    <form class="team_compare" method="post" action="" name="form">
        <div>
        <p><label>Away Team</label></p>
        <select name="team_1">
            <option value="-1">Select a team</option>

            <?php
            foreach ($teams as $team){
                echo "<option value='" . $team . "'>" . getTeamName($team). "</option>";
            }
            ?>
        </select>
        </div>
        <div>
            <p><label>Home Team</label></p>
        <select name="team_2">
            <option value="-2">Select a team</option>

            <?php
            foreach ($teams as $team){
                echo "<option value='" . $team . "'>" . getTeamName($team). "</option>";
            }
            ?>
        </select>
        </div>
        <p><input name="submit" type="submit" value="Compare"></p>
    </form>

<?php
if (isset($_POST['team_1']) or isset($_POST['team_2'])) {
    if ($_POST['team_1'] == -1 or $_POST['team_2'] == -2) {
        $message = "You must select two teams!";
        echo "<script type='text/javascript'>alert('$message');</script>";
    } elseif ($_POST['team_1'] == $_POST['team_2']) {
        $message = "You must select unique teams!";
        echo "<script type='text/javascript'>alert('$message');</script>";
    } else {
        $away_team = $_POST['team_1'];
        $home_team = $_POST['team_2'];

        echo "<br><table><tr align='center'>
        <th>Away</th>
        <th>Home</th>
        <th>Predicted Winner</th>
        <th>Predicted Spread</th>
    </tr>";

        echo '<tr align="center">';
        echo "<td>" . getTeamName($away_team) . "</td>";
        echo "<td>" . getTeamName($home_team) . "</td>";

        echo "<td>";
        if($win_loss_matrix[$home_team][$away_team] == 1)
            echo "Home";
        else
            echo "Away";
        echo"</td>";

        echo "<td>" . $spreads[$home_team][$away_team] . "</td>";
        echo "</tr>";
        echo "</table>";
    }
}

    ?>

</body>
</html>