function [prediction,spread] = matchupPrediction(predictionModel,spreadModel,homeTeam,awayTeam,runningData)

    numInterestAttributes = 41;    
    gameData = [runningData(homeTeam,:) runningData(awayTeam,:)];
    
    homeGameCol = gameData(41);
    awayGameCol = gameData(84);
    
    %divide per/game statistics by the number of games (yards, points, etc.)
    for gameDivAttr = [1:8 21:28 42:43]
        gameData(gameDivAttr) = gameData(gameDivAttr)./homeGameCol;
        gameData(gameDivAttr+numInterestAttributes+2) = gameData(gameDivAttr+numInterestAttributes+2)./awayGameCol;
    end

    %divide the pass length numbers by total passes to get ratios
    for passLenAttr = [9 29]
        passLenSumCol = gameData(:,passLenAttr) + gameData(:,passLenAttr+1);
        gameData(passLenAttr) = gameData(passLenAttr)./passLenSumCol;
        gameData(passLenAttr+1) = gameData(passLenAttr+1)./passLenSumCol;
        passLenSumCol = gameData(:,passLenAttr+numInterestAttributes+2) + gameData(:,passLenAttr+1+numInterestAttributes+2);
        gameData(passLenAttr+numInterestAttributes+2) = gameData(passLenAttr+numInterestAttributes+2)./passLenSumCol;
        gameData(passLenAttr+1+numInterestAttributes+2) = gameData(passLenAttr+1+numInterestAttributes+2)./passLenSumCol;
    end

    %divide the pass location numbers by total passes to get ratios
    for passLocAttr = [11 31]
        passLocSumCol = gameData(:,passLocAttr) + gameData(:,passLocAttr+1) + gameData(:,passLocAttr+2);
        gameData(passLocAttr) = gameData(passLocAttr)./passLocSumCol;
        gameData(passLocAttr+1) = gameData(passLocAttr+1)./passLocSumCol;
        gameData(passLocAttr+2) = gameData(passLocAttr+2)./passLocSumCol;
        passLocSumCol = gameData(:,passLocAttr+numInterestAttributes+2) + gameData(:,passLocAttr+1+numInterestAttributes+2) + gameData(:,passLocAttr+2+numInterestAttributes+2);
        gameData(passLocAttr+numInterestAttributes+2) = gameData(passLocAttr+numInterestAttributes+2)./passLocSumCol;
        gameData(passLocAttr+1+numInterestAttributes+2) = gameData(passLocAttr+1+numInterestAttributes+2)./passLocSumCol;
        gameData(passLocAttr+2+numInterestAttributes+2) = gameData(passLocAttr+2+numInterestAttributes+2)./passLocSumCol;
    end

    %divide the rush gap numbers by total rushes to get ratios
    for rushGapAttr = [14 34]
        rushGapSumCol = gameData(:,rushGapAttr) + gameData(:,rushGapAttr+1) + gameData(:,rushGapAttr+2) + gameData(:,rushGapAttr+3);
        gameData(rushGapAttr) = gameData(rushGapAttr)./rushGapSumCol;
        gameData(rushGapAttr+1) = gameData(rushGapAttr+1)./rushGapSumCol;
        gameData(rushGapAttr+2) = gameData(rushGapAttr+2)./rushGapSumCol;
        gameData(rushGapAttr+3) = gameData(rushGapAttr+3)./rushGapSumCol;
        rushGapSumCol = gameData(:,rushGapAttr+numInterestAttributes+2) + gameData(:,rushGapAttr+1+numInterestAttributes+2) + gameData(:,rushGapAttr+2+numInterestAttributes+2) + gameData(:,rushGapAttr+3+numInterestAttributes+2);
        gameData(rushGapAttr+numInterestAttributes+2) = gameData(rushGapAttr+numInterestAttributes+2)./rushGapSumCol;
        gameData(rushGapAttr+1+numInterestAttributes+2) = gameData(rushGapAttr+1+numInterestAttributes+2)./rushGapSumCol;
        gameData(rushGapAttr+2+numInterestAttributes+2) = gameData(rushGapAttr+2+numInterestAttributes+2)./rushGapSumCol;
        gameData(rushGapAttr+3+numInterestAttributes+2) = gameData(rushGapAttr+3+numInterestAttributes+2)./rushGapSumCol;
    end

    %divide the rush location numbers by total rushes to get ratios
    for rushLocAttr = [18 38]
        rushLocSumCol = gameData(:,rushLocAttr) + gameData(:,rushLocAttr+1) + gameData(:,rushLocAttr+2);
        gameData(rushLocAttr) = gameData(rushLocAttr)./rushLocSumCol;
        gameData(rushLocAttr+1) = gameData(rushLocAttr+1)./rushLocSumCol;
        gameData(rushLocAttr+2) = gameData(rushLocAttr+2)./rushLocSumCol;
        rushLocSumCol = gameData(:,rushLocAttr+numInterestAttributes+2) + gameData(:,rushLocAttr+1+numInterestAttributes+2) + gameData(:,rushLocAttr+2+numInterestAttributes+2);
        gameData(rushLocAttr+numInterestAttributes+2) = gameData(rushLocAttr+numInterestAttributes+2)./rushLocSumCol;
        gameData(rushLocAttr+1+numInterestAttributes+2) = gameData(rushLocAttr+1+numInterestAttributes+2)./rushLocSumCol;
        gameData(rushLocAttr+2+numInterestAttributes+2) = gameData(rushLocAttr+2+numInterestAttributes+2)./rushLocSumCol;
    end
    
    prediction = int32(cellfun(@str2num,predict(predictionModel,gameData)));
    spread = predict(spreadModel,gameData);
    
end